import java.util.Random;

class IntegerPair {
	private int a;
	private int b;

	public IntegerPair(int a, int b) {
		this.a = a;
		this.b = b;
	}

	public int getA() {
		return a;
	}
	public void setA(int a) {
		this.a = a;
	}
	public int getB() {
		return b;
	}
	public void setB(int b) {
		this.b = b;
	}
}


public class Document {
	private int id = -1;
	private int[] tokens = null;
	public String filename = "";
	public double sim = 0.0;
	private static Random rnd = new Random(System.currentTimeMillis());

	public Document(Integer[] tokens, String filename, int id) {
		if (tokens != null) {
			if (this.tokens == null)
				this.tokens = new int[tokens.length];
			for (int i=0 ; i < tokens.length ; i++) {
				this.tokens[i] = tokens[i];
			}
		}
		this.filename = filename;
		this.id = id;
	}


	private IntegerPair hashFuncGen(){
		// New random
		// Random values from {1..100}
		int a = rnd.nextInt(100) + 1;
		int b = rnd.nextInt(100) + 1;
		// Initialise pair
		IntegerPair pair = new IntegerPair(a, b);
		// Return pair
		return pair;
	}


	private long hash(IntegerPair ip, int x) {
		// Return the hash
		return Math.round(( ip.getA() * x + ip.getB() ) % 52.981);
	}


	public double minhash(Document doc) {
		int hashes = 0;
		int repetitions = 100;
		int cntdoc = doc.tokens.length;
		int cntthis = this.tokens.length;
		// Loop different hash functions
		for(int i=1; i<=repetitions; i++){
			// Initialise pair of integers
			IntegerPair pair = hashFuncGen();

			long hdoc = 0;
			long mdoc = 99999999;
			// Get random tokens
			for(int j=0;j<cntdoc/3;j++){
				// Compute hash
				hdoc = hash(pair, doc.tokens[rnd.nextInt(cntdoc)]);
				// Find minimum hash value
				if(hdoc < mdoc){
					mdoc = hdoc;
				}
			}
			long hthis = 0;
			long mthis = 99999999;
			// Get 100 random tokens
			for(int k=0;k<cntthis/3;k++){
				// Compute hash
				hthis = hash(pair, this.tokens[rnd.nextInt(cntthis)]);
				// Find minimum hash value
				if(hthis < mthis){
					mthis = hthis;
				}
			}
			// If minimum hash values match
			if( mthis == mdoc ){
				// Increment hashes
				hashes++;
			}
		}
		return (double)hashes/repetitions;
	}


	public double jaccard(Document doc) {
		int tokens = 0;
		// Loop tokens in doc
		for(int doc_token : doc.tokens ){
			// Loop tokens in this
			for(int this_token : this.tokens ){
				// If tokens match
				if( doc_token == this_token ){
					// Increment tokens
					tokens++;
					// And break
					break;
				}
			}
		}
		// Return Jaccard value
		return (double)tokens/(doc.tokens.length + this.tokens.length - tokens);
	}
}
