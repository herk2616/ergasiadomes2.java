public class Heap {

	private Document[] heapArray;
	private int maxSize;           // size of array
	private int currentSize;       // number of nodes in array
	private Document userDoc;      // users document of preference

	public Heap(int mx, Document userDoc){
		maxSize = mx;
		currentSize = 0;
		heapArray = new Document[maxSize];
		this.userDoc = userDoc;
	}


	public boolean insert(Document doc){
		int i = 0;
		// If empty
		if(currentSize == 0){
			// doc is the first document
			heapArray[0] = doc;
			// Increment current size
			currentSize++;
			return true;
		// If full
		}else if(currentSize == maxSize){
			// And if doc is more similar
			if(doc.sim > heapArray[currentSize-1].sim){
				// Call removeMin
				removeMin();
				// And insert doc
				insert(doc);
				return true;
			// Else exit with false
			}else{
				return false;
			}
		// If not empty
		}else{
			// Loop heap from last-1 to 0
			for(i = currentSize-1; i >= 0; i--){
				// If new document is more similar
				if( doc.sim > heapArray[i].sim){
					// Shift
					heapArray[i+1] = heapArray[i];
				// Else
				}else{
					break;
				}
			}
			// Insert doc
			heapArray[i+1] = doc;
			// increment size
			currentSize++;
			return true;
		}
	}


	public Document removeMin(){
		return this.heapArray[--currentSize];
	}


	public Document remove(){
		if(this.currentSize > 0){
			Document max = this.heapArray[0];
			for(int i=0; i<this.currentSize-1; i++){
				this.heapArray[i] = this.heapArray[i+1];
			}
				currentSize--;
			return max;
		}else{
			return null;
		}
	}


	private void simulate(double thresh, int mtu, Document[] doc){
		int count = 0; 		// Document count
		int cntMinhash = 0; 	// Minhash count
		int cntJaccard = 0; 	// Jaccard count

		// for in loop
		for(Document d : doc){
			// Find simmilarity with minhash
			d.sim = d.minhash(this.userDoc);
			// Insert in heap
			this.insert(d);
			// Count
			count++;
			// If jaccard > thresh
			if(d.jaccard(this.userDoc)>thresh){
				// Count
				cntJaccard++;
			}
			// When news reach news per day limit
			if(count % mtu == 0){
				// Remove most accurate news
				Document removed = this.remove();
				// Compute minhash 10 times && fing avg val
				double minhashvalue = 0;
				for(int k=0;k<10;k++){
					minhashvalue = minhashvalue + removed.minhash(this.userDoc)/10;
				}
				// If avg val > thresh
				if(minhashvalue > thresh){
					// Count
					cntMinhash++;
				}
			}
		}
		// Print results
		System.out.printf("threshold = %1.2f n = %2d", thresh, mtu);
		System.out.printf(" accuracy = %8.5f \n",
				(double)cntMinhash/Math.min(cntJaccard,8000/mtu));
	}


	public static void main(String[] args){
		// New parser
		ReutersParser rout = new ReutersParser("./data");
		// Document list
		Document[] doc = rout.parse();
		// Users Document of preference
		Document userDoc = doc[Integer.parseInt(args[0])];
		// Threshold
		for(double thresh=0.10;thresh<=0.30;thresh+=0.05){
			// News per day
			for(int mtu=2;mtu<=10;mtu+=2){
				Heap newHeap = new Heap(10, userDoc);
				newHeap.simulate(thresh, mtu, doc);
			}
		}
	}   // End of main
}   // End of class

