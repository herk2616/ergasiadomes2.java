---
title: "Δομές Δεδομένων 2η Εργασία"
author: "Irakleios Kopitas"
date: "30/12/2019"
geometry: margin=3cm
output: pdf_document
header-includes:
- \setlength{\parindent}{1em}
- \setlength{\parskip}{0em}
---


![Unipi](http://www.unipi.gr/unipi/odigos_spoudon/unipilogo.png)

\pagebreak


# Resaults

## Document 124

### Data

\begin{center}\rule{4.8in}{0.4pt}\end{center}

```C
                     threshold = 0.10 n =  2 accuracy =  1.08021
                     threshold = 0.10 n =  4 accuracy =  0.97550
                     threshold = 0.10 n =  6 accuracy =  0.98650
                     threshold = 0.10 n =  8 accuracy =  0.99200
                     threshold = 0.10 n = 10 accuracy =  0.98750
                     threshold = 0.15 n =  2 accuracy =  1.60424
                     threshold = 0.15 n =  4 accuracy =  0.89945
                     threshold = 0.15 n =  6 accuracy =  0.61624
                     threshold = 0.15 n =  8 accuracy =  0.53700
                     threshold = 0.15 n = 10 accuracy =  0.55250
                     threshold = 0.20 n =  2 accuracy =  0.01923
                     threshold = 0.20 n =  4 accuracy =  0.01923
                     threshold = 0.20 n =  6 accuracy =  0.01923
                     threshold = 0.20 n =  8 accuracy =  0.00641
                     threshold = 0.20 n = 10 accuracy =  0.01282
                     threshold = 0.25 n =  2 accuracy =  0.00000
                     threshold = 0.25 n =  4 accuracy =  0.06667
                     threshold = 0.25 n =  6 accuracy =  0.00000
                     threshold = 0.25 n =  8 accuracy =  0.06667
                     threshold = 0.25 n = 10 accuracy =  0.06667
                     threshold = 0.30 n =  2 accuracy =  0.00000
                     threshold = 0.30 n =  4 accuracy =  0.00000
                     threshold = 0.30 n =  6 accuracy =  0.00000
                     threshold = 0.30 n =  8 accuracy =  0.00000
                     threshold = 0.30 n = 10 accuracy =  0.00000
```

\begin{center}\rule{4.8in}{0.4pt}\end{center}

### Graph


![124](./results/124.pdf){width=100%}

\pagebreak

## Document 128

### Data

\begin{center}\rule{4.8in}{0.4pt}\end{center}

```C
                     threshold = 0.10 n =  2 accuracy =  1.19897
                     threshold = 0.10 n =  4 accuracy =  0.91000
                     threshold = 0.10 n =  6 accuracy =  0.93098
                     threshold = 0.10 n =  8 accuracy =  0.94500
                     threshold = 0.10 n = 10 accuracy =  0.93875
                     threshold = 0.15 n =  2 accuracy =  0.09884
                     threshold = 0.15 n =  4 accuracy =  0.05648
                     threshold = 0.15 n =  6 accuracy =  0.05135
                     threshold = 0.15 n =  8 accuracy =  0.04236
                     threshold = 0.15 n = 10 accuracy =  0.03338
                     threshold = 0.20 n =  2 accuracy =  0.00917
                     threshold = 0.20 n =  4 accuracy =  0.01835
                     threshold = 0.20 n =  6 accuracy =  0.00917
                     threshold = 0.20 n =  8 accuracy =  0.00917
                     threshold = 0.20 n = 10 accuracy =  0.00917
                     threshold = 0.25 n =  2 accuracy =  0.00000
                     threshold = 0.25 n =  4 accuracy =  0.06250
                     threshold = 0.25 n =  6 accuracy =  0.00000
                     threshold = 0.25 n =  8 accuracy =  0.00000
                     threshold = 0.25 n = 10 accuracy =  0.06250
                     threshold = 0.30 n =  2 accuracy =  0.00000
                     threshold = 0.30 n =  4 accuracy =  0.00000
                     threshold = 0.30 n =  6 accuracy =  0.00000
                     threshold = 0.30 n =  8 accuracy =  0.00000
                     threshold = 0.30 n = 10 accuracy =  0.00000
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

### Graph

![128](./results/128.pdf){width=100%}

\pagebreak

## Document 1423

### Data

\begin{center}\rule{4.8in}{0.4pt}\end{center}
```C
                     threshold = 0.10 n =  2 accuracy =  1.23970
                     threshold = 0.10 n =  4 accuracy =  0.98950
                     threshold = 0.10 n =  6 accuracy =  0.99325
                     threshold = 0.10 n =  8 accuracy =  0.99700
                     threshold = 0.10 n = 10 accuracy =  0.99875
                     threshold = 0.15 n =  2 accuracy =  5.92693
                     threshold = 0.15 n =  4 accuracy =  3.45720
                     threshold = 0.15 n =  6 accuracy =  2.36952
                     threshold = 0.15 n =  8 accuracy =  1.83507
                     threshold = 0.15 n = 10 accuracy =  1.49269
                     threshold = 0.20 n =  2 accuracy = 22.76923
                     threshold = 0.20 n =  4 accuracy = 17.07692
                     threshold = 0.20 n =  6 accuracy = 11.07692
                     threshold = 0.20 n =  8 accuracy =  8.61538
                     threshold = 0.20 n = 10 accuracy =  8.00000
                     threshold = 0.25 n =  2 accuracy =  1.00000
                     threshold = 0.25 n =  4 accuracy =  1.00000
                     threshold = 0.25 n =  6 accuracy =  1.00000
                     threshold = 0.25 n =  8 accuracy =  0.00000
                     threshold = 0.25 n = 10 accuracy =  1.00000
                     threshold = 0.30 n =  2 accuracy =  0.00000
                     threshold = 0.30 n =  4 accuracy =  0.00000
                     threshold = 0.30 n =  6 accuracy =  0.00000
                     threshold = 0.30 n =  8 accuracy =  0.00000
                     threshold = 0.30 n = 10 accuracy =  0.00000
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

### Graph

![1423](./results/1423.pdf){width=100%}

\pagebreak

# Heap

## Insert Description

Αυτός είναι ο κώδικας της μεθόδου insert για την κλάση heap, την καλουμε για να
προσθέσουμε ένα αρχείο στον σωρό κρατώντας σειρά ως προς την ομοιότητα με
προεπιλεγμένο αρχείο. Διακρίνουμε τρεις περιπτώσεις.

Αν η heap είναι γεμάτη τότε, αν η ομοιότητα του αρχείου είναι μεγαλύτερη από
το τελευταίο, καλούμε την delete και το προσθέτουμε ξανακαλώντας την insert.

Αν η heap είναι άδεια τότε το αρχείο πάει στην θέση [0].

Τελος στην τελευταία περίπτωση λουπάρουμε από το τέλος και κάθε φορά που
συναντάμε αρχείο λιγότερο όμοιο το βάζουμε μια θέση ποιο πίσω, όταν βρούμε ένα
ποιο όμοιο αρχείο από αυτό που θέλουμε να προσθέσουμε σταματάμε και βάζουμε το
κείμενο στην κενή θέση που έχει δημιουργηθεί.

Η πολυπλοκότητα της μεθόδου ειναι Ο(n).

\pagebreak

## Insert Code

\begin{center}\rule{4.8in}{0.4pt}\end{center}
```java
	public boolean insert(Document doc){
		int i = 0;
		// If empty
		if(currentSize == 0){
			// doc is the first document
			heapArray[0] = doc;
			// Increment current size
			currentSize++;
			return true;
		// If full
		}else if(currentSize == maxSize){
			// And if doc is more similar
			if(doc.sim > heapArray[currentSize-1].sim){
				// Call removeMin
				removeMin();
				// And insert doc
				insert(doc);
				return true;
			// Else exit with false
			}else{
				return false;
			}
		// If not empty
		}else{
			// Loop heap from last-1 to 0
			for(i = currentSize-1; i >= 0; i--){
				// If new document is more similar
				if( doc.sim > heapArray[i].sim){
					// Shift
					heapArray[i+1] = heapArray[i];
				// Else
				}else{
					break;
				}
			}
			// Insert doc
			heapArray[i+1] = doc;
			// increment size
			currentSize++;
			return true;
		}
	}
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}
\pagebreak

## RemoveMin Description

Η removeMin αφαιρεί από τον σωρό το λιγότερο όμοιο αρχείο. Αξιοποιεί την θέση του
αρχείου στον σωρό και πετυχαίνει διαγραφή μειώνοντας την Τιμή του currentSize.

Η πολυπλοκότητα της μεθόδου ειναι Ο(1).


## RemoveMin Code

\begin{center}\rule{4.8in}{0.4pt}\end{center}
```java
	public Document removeMin(){
		return this.heapArray[--currentSize];
	}
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}


## Remove Description

Η remove αφαιρεί από τον σωρό το ποιο όμοιο αρχείο. Η θέση του
αρχείου στον σωρό είναι η [0] οπότε για να πετύχει διαγραφή χωρίς να αλλοιώσει
την εσωτερική δομή του σωρού διατρέχει όλα τα στοιχεία μεταφέροντας τα μια θέση
ποιο μπροστά επικαλύπτοντας το αρχείο.

Η πολυπλοκότητα της μεθόδου ειναι Ο(n).

## Remove Code

\begin{center}\rule{4.8in}{0.4pt}\end{center}
```java
	public Document remove(){
		if(this.currentSize > 0){
			Document max = this.heapArray[0];
			for(int i=0; i<this.currentSize-1; i++){
				this.heapArray[i] = this.heapArray[i+1];
			}
			currentSize--;
			return max;
		}else{
			return null;
		}
	}
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}


\pagebreak


