thresh010 = [
 1.08565
 0.97950
 0.98575
 0.99100
 0.98750
]

thresh015 = [
 1.64668
 0.89391
 0.65867
 0.56800
 0.56125
]

thresh020 = [
 0.01282
 0.00641
 0.01282
 0.01923
 0.01282
]

thresh025 = [
 0.06667
 0.00000
 0.00000
 0.06667
 0.06667
]

thresh030 = [
 0.00000
 0.00000
 0.00000
 0.00000
 0.00000
]
y = [ 2 4 6 8 10 ]
subplot(2,3,1)
plot(y, thresh010)
title('thresh = 0.10')
ylabel('accurasy')
xlabel('n')
subplot(2,3,2)
plot(y, thresh015)
title('thresh = 0.15')
ylabel('accurasy')
xlabel('n')
subplot(2,3,3)
plot(y, thresh020)
title('thresh = 0.20')
ylabel('accurasy')
xlabel('n')
subplot(2,3,4)
plot(y, thresh025)
title('thresh = 0.25')
ylabel('accurasy')
xlabel('n')
subplot(2,3,6)
plot(y, thresh030)
title('thresh = 0.30')
ylabel('accurasy')
xlabel('n')
print('124.pdf')
