thresh010 = [
 1.19244
 0.91750
 0.92498
 0.94400
 0.93250
]

thresh015 = [
 0.13607
 0.05777
 0.04236
 0.05006
 0.03466
]

thresh020 = [
 0.00917
 0.01835
 0.00917
 0.00917
 0.00917
]

thresh025 = [
 0.06250
 0.00000
 0.00000
 0.00000
 0.00000
]

thresh030 = [
 0.00000
 0.00000
 0.00000
 0.00000
 0.00000
]
y = [ 2 4 6 8 10 ]
subplot(2,3,1)
plot(y, thresh010)
title('thresh = 0.10')
ylabel('accurasy')
xlabel('n')
subplot(2,3,2)
plot(y, thresh015)
title('thresh = 0.15')
ylabel('accurasy')
xlabel('n')
subplot(2,3,3)
plot(y, thresh020)
title('thresh = 0.20')
ylabel('accurasy')
xlabel('n')
subplot(2,3,4)
plot(y, thresh025)
title('thresh = 0.25')
ylabel('accurasy')
xlabel('n')
subplot(2,3,6)
plot(y, thresh030)
title('thresh = 0.30')
ylabel('accurasy')
xlabel('n')
print('128.pdf')

