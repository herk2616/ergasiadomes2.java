thresh010 = [
 1.24421
 0.99050
 0.99700
 0.99900
 0.99875
]

thresh015 = [
 5.92693
 3.40501
 2.40084
 1.85177
 1.49687
]

thresh020 = [
23.15385
14.61538
 9.69231
 9.46154
 7.23077
]

thresh025 = [
 1.00000
 1.00000
 1.00000
 1.00000
 0.00000
]

thresh030 = [
 0.00000
 0.00000
 0.00000
 0.00000
 0.00000
]
y = [ 2 4 6 8 10 ]
subplot(2,3,1)
plot(y, thresh010)
title('thresh = 0.10')
ylabel('accurasy')
xlabel('n')
subplot(2,3,2)
plot(y, thresh015)
title('thresh = 0.15')
ylabel('accurasy')
xlabel('n')
subplot(2,3,3)
plot(y, thresh020)
title('thresh = 0.20')
ylabel('accurasy')
xlabel('n')
subplot(2,3,4)
plot(y, thresh025)
title('thresh = 0.25')
ylabel('accurasy')
xlabel('n')
subplot(2,3,6)
plot(y, thresh030)
title('thresh = 0.30')
ylabel('accurasy')
xlabel('n')
print('1423.pdf')
