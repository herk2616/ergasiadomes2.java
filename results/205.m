thresh010 = [
 1.05025
 0.99900
 0.99925
 1.00000
 1.00000
]

thresh015 = [
 8.47087
 4.63835
 3.19417
 2.40777
 1.93447
]

thresh020 = [
154.25000
98.06250
71.93750
56.12500
46.56250
]

thresh025 = [
648.50000
477.50000
379.00000
314.00000
267.50000
]

thresh030 = [
192.00000
156.00000
130.00000
110.00000
105.00000
]
y = [ 2 4 6 8 10 ]
subplot(2,3,1)
plot(y, thresh010)
title('thresh = 0.10')
ylabel('accurasy')
xlabel('n')
subplot(2,3,2)
plot(y, thresh015)
title('thresh = 0.15')
ylabel('accurasy')
xlabel('n')
subplot(2,3,3)
plot(y, thresh020)
title('thresh = 0.20')
ylabel('accurasy')
xlabel('n')
subplot(2,3,4)
plot(y, thresh025)
title('thresh = 0.25')
ylabel('accurasy')
xlabel('n')
subplot(2,3,6)
plot(y, thresh030)
title('thresh = 0.30')
ylabel('accurasy')
xlabel('n')
print('205.pdf')
